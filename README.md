# Welcome to my library!

##### Most of the pdf's had been downloaded from gen.lib.rus.ec

#### The books are ordered either by topic or by publisher.

#### Here we go!
---

## No starch press
* Automate the boring stuff with python - Al Sweigart
* Invent your own computer games with python - Al Sweigart
* Doing math with python - Amit Saha
* How Linux works - Brian Ward
* Learn Java the easy way - Bryson Payne
* The TCP IP guide - Charles Kozierok
* Land of Lisp - Conrad Barski M.D.
* Clojure for the brave and true - Daniel Higginbotham
* The book of Qt4 - Daniel Molkentin
* Wicked cool shell scripts - Taylor Perry
* Python crash course - Eric Matthes
* Ruby wizardry - Eric Weinstein
* The book of ruby - Huw Collingbourne
* Attacking network protocols - James Forshaw
* Serious criptography - Jean-Philipe Aumasson
* The GNU make book - John Graham
* Network know-how - John Ross Dr.
* Hacking, The art of exploitation - Jon Erickson
* Black hat python - Justin Seitz

<https://mega.nz/#F!BNg3wYjL!f4VCPWvCc1YMCVtImOAZVQ>


